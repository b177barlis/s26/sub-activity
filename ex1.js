// create a variable port and assign it with the value of 4001
// create a server using the createServer method that will listen in to the port provided.
// console log in the terminal a message when the server is successfully running.
// create a condition that when in the /welcome route is accessed, it will print a message to the user saying "Welcome to the world of Node.js"
// Create another condition that when in the /register route is accessed, it will print a message to the user saying "Page is under maintenance" with status code of 503.
// Create a last condition that will return an error message if accessing other routes.


let http = require("http");

http.createServer((request,response) =>{
	if(request.url == '/welcome'){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end(`Welcome to the world of Node.js`); }
	else if(request.url == '/register'){
		response.writeHead(504, {'Content-Type': 'text/plain'})
		response.end(`Page is under maintenance. Error ${response.statusCode}.`); }
	else{
		response.end(`This page is not available.`);}
}).listen(4001);

console.log('Server is running');