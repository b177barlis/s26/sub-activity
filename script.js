// Create an odd-even checker that will check which numbers from 1-300 are odd amd which are even.



for (let number = 1; number <= 300; number++) {
    if (number % 2 === 0) {
        console.log(`${number} - even`);
    }
    else {
        console.log(`${number} - odd`);
    }

}