// average stock
// Create an aggregate that will get the average of stocks of fruits on sale.
// Get the average stocks of fruits based on specific suppliers.

db.fruits.aggregate([
			{ $match: {onSale: true}},
			{ $group: { _id: "$supplier_id", avg_stock: { $avg: "$stock"}}}]);


