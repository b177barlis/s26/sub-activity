// max price
// Create an aggregate that will get the maximum price of fruits on sale.
// Get the max price in origin "Philippines".

db.fruits.aggregate([
		{ $match: { $and: [{ onSale: true }, { origin: "Philippines" }] }},
		{$group: {_id: "Philippines", max_price: {$max: "$price"}}}
		]);